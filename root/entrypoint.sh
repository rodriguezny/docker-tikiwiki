#!/bin/bash

export USERNAME=`awk -F: '{ if ($3 == '$UID') { print $1 } }' /etc/passwd`
export COMPOSER_CACHE_DIR="/tmp/$USERNAME/composer"

if [ -n "$XDEBUG" ];
then
    inifile="/usr/local/etc/php/conf.d/pecl-xdebug.ini"
    extfile="$(find /usr/local/lib/php/extensions/ -name xdebug.so)";
    client_port="${XDEBUG_CLIENT_PORT:-9003}";
    client_os="${XDEBUG_CLIENT_OS:-linux}";
    idekey="${XDEBUG_IDEKEY:-xdbg}";

    if [ -f "$extfile" ] && [ ! -f "$inifile" ];
    then
        {
            echo "[Xdebug]";
            echo "zend_extension=${extfile}";
            echo "xdebug.idekey=${idekey}";
            echo "xdebug.mode=debug";
            echo "xdebug.start_with_request=yes";
            echo "xdebug.client_port=${client_port}";
        } > $inifile;

        if [ $client_os = "macos" ] || [ $client_os = "windows" ];
        then
            {
                echo "xdebug.discover_client_host=false";
                echo "xdebug.client_host=host.docker.internal";
            } >> $inifile;
        else
            {
                echo "xdebug.discover_client_host=true";
            } >> $inifile;
        fi

    fi

    unset extfile client_port idekey;
fi

if [ "$UID" = "0" ];
then
    if [[ "$WWW_DATA_ID" =~ ^([0-9][0-9]*):([0-9][0-9]*)$ ]];
    then
        usermod -u "${BASH_REMATCH[1]}" www-data
        groupmod -g "${BASH_REMATCH[2]}" www-data
        unset "WWW_DATA_ID"
    fi

    if [[ "$WWW_DATA_UID" =~ ^([0-9][0-9]*)$ ]];
    then
        usermod -u "${BASH_REMATCH[1]}" www-data
        unset "WWW_DATA_UID"
    fi

    if [[ "$WWW_DATA_GID" =~ ^([0-9][0-9]*)$ ]];
    then
        groupmod -g "${BASH_REMATCH[1]}" www-data
        unset "WWW_DATA_GID"
    fi
fi

if [ -d "/entrypoint.d" ];
then
    for extra in /entrypoint.d/*; do
        case "$extra" in
            *.sh)     . "$extra" ;;
            *.php)    /usr/local/bin/php  "$extra" ;;
        esac
        echo
    done
    unset extra
fi

exec "$@"
